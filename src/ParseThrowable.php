<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-parser-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Parser;

use Throwable;

/**
 * ParseThrowable interface file.
 * 
 * This represents an exception that a parser may throw when it fails to
 * interpret data as valid regarding its internal representation.
 * 
 * @author Anastaszor
 */
interface ParseThrowable extends Throwable
{
	
	/**
	 * Gets the offset on the data that failed parsing through the original
	 * data structure.
	 *
	 * @return integer
	 */
	public function getOffset() : int;
	
	/**
	 * Gets a string representation of the data that failed parsing. This data
	 * is not made to be used automatically, but as help for humans to find
	 * the right problem when the parsing was done and has failed.
	 *
	 * @return ?string
	 */
	public function getData() : ?string;
	
	/**
	 * Gets the classname in which the data wanted to be parsed to.
	 *
	 * @return class-string
	 */
	public function getClassname() : string;
	
}
