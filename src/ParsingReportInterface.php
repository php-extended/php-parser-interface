<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-parser-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Parser;

use Countable;
use Iterator;
use Stringable;

/**
 * ParsingReportInterface interface file.
 * 
 * This is a report on parsing errors that occured during the parsing process.
 * 
 * @author Anastaszor
 * @extends \Iterator<integer, ParsingReportEntryInterface>
 */
interface ParsingReportInterface extends Countable, Iterator, Stringable
{
	
	/**
	 * Adds a new entry to the list of entries for this report.
	 * 
	 * @param ParsingReportEntryInterface $entry
	 * @return ParsingReportInterface
	 */
	public function addEntry(ParsingReportEntryInterface $entry) : ParsingReportInterface;
	
	/**
	 * Removes all the entries of this report. If a classname is given, then
	 * removes only the entries that were reported for the given classname.
	 * 
	 * @param ?string $classname
	 * @return integer the number of entries removed
	 */
	public function clear(?string $classname = null) : int;
	
}
