<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-parser-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Parser;

use Stringable;

/**
 * ParsingReportEntryInterface interface file.
 * 
 * This represents an entry of a failure to be parsed for a given chunk of data.
 * This object remembers information about the chunk of data that failed parsing
 * for later analysis.
 * 
 * @author Anastaszor
 */
interface ParsingReportEntryInterface extends Stringable
{
	
	/**
	 * Gets the index of the object that failed parsing into the iteration.
	 *
	 * @return integer
	 */
	public function getIndex() : int;
	
	/**
	 * Gets the offset on the data that failed parsing through the original
	 * data structure.
	 * 
	 * @return integer
	 */
	public function getOffset() : int;
	
	/**
	 * Gets a string representation of the data that failed parsing. This data
	 * is not made to be used automatically, but as help for humans to find
	 * the right problem when the parsing was done and has failed.
	 * 
	 * @return ?string
	 */
	public function getData() : ?string;
	
	/**
	 * Gets the class of the object that failed to be parsed.
	 * 
	 * @return class-string
	 */
	public function getClassname() : string;
	
	/**
	 * Gets the message of the parsing failure.
	 * 
	 * @return string
	 */
	public function getMessage() : string;
	
}
