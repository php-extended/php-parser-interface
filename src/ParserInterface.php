<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-parser-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Parser;

use Iterator;
use Stringable;

/**
 * ParserInterface interface file.
 * 
 * This interface represents a single parser capabilities. A parser is an object
 * that transforms strings or streams of data into object form.
 * 
 * @author Anastaszor
 * @template T of object
 */
interface ParserInterface extends Stringable
{
	
	/**
	 * Parses the given data into the object it represents. If the parsing
	 * cannot be done, then a ParseThrowable is thrown.
	 * 
	 * @param string $data
	 * @return T
	 * @throws ParseThrowable if the parsing process fails
	 */
	public function parse(?string $data) : object;
	
	/**
	 * Parses the given data into a the object it represents. If a nullable
	 * value is represented, then a null is returned, otherwise if the parsing
	 * cannot be done, then a ParseThrowable is thrown.
	 * 
	 * @param string $data
	 * @return ?T
	 * @throws ParseThrowable if the parsing process fails
	 */
	public function parseNullable(?string $data) : ?object;
	
	/**
	 * Parses the given data into the object it represents. If the parsing
	 * cannot be done, then a null value is returned.
	 * 
	 * If a parsing report is given, it will be filled with all the errors
	 * that are encountered when parsing those objects.
	 * 
	 * @param string $data
	 * @param ?ParsingReportInterface $report
	 * @param integer $idx the index to be put in the report, if done in loop
	 * @return ?T
	 */
	public function tryParse(?string $data, ?ParsingReportInterface $report = null, int $idx = 0) : ?object;
	
	/**
	 * Parses the given data into the object it represents. If a nullable
	 * value is represented, or if the parsing cannot be done, then a null
	 * is returned.
	 * 
	 * If a parsing report is given, it will be filled with all the errors
	 * that are encountered when parsing those objects.
	 * 
	 * @param string $data
	 * @param ?ParsingReportInterface $report
	 * @param integer $idx the index to be put in the report, if done in loop
	 * @return ?T
	 */
	public function tryParseNullable(?string $data, ?ParsingReportInterface $report = null, int $idx = 0) : ?object;
	
	/**
	 * Parses the given datas into the objects they represent. If one of the
	 * given datas cannot be parsed, then a ParseThrowable is thrown.
	 * 
	 * @param array<integer|string, ?string> $datas
	 * @return array<integer, T>
	 * @throws ParseThrowable if the parsing process fails
	 */
	public function parseAll(array $datas) : array;
	
	/**
	 * Parses the given datas into the objects they represent. If a nullable
	 * value is represented, then it will not be included into the resulting
	 * array. If one of the given datas cannot be parsed, then a ParseThrowable
	 * is thrown.
	 * 
	 * @param array<integer|string, ?string> $datas
	 * @return array<integer, T>
	 * @throws ParseThrowable if the parsing process fails
	 */
	public function parseAllNullable(array $datas) : array;
	
	/**
	 * Parses the given datas into the objects they represent. If one the the
	 * given datas cannot be parsed, then it will silently be discarded from
	 * the resulting array.
	 * 
	 * If a parsing report is given, it will be filled with all the errors
	 * that are encountered when parsing those objects.
	 * 
	 * @param array<integer|string, ?string> $datas
	 * @param ?ParsingReportInterface $report
	 * @return array<integer, T>
	 */
	public function tryParseAll(array $datas, ?ParsingReportInterface $report = null) : array;
	
	/**
	 * Parses the given datas into the objects they represent. If one of the
	 * given datas cannot be parsed, then it will silently be discarded from
	 * the resulting array. If a nullable value is given, it will also not be
	 * included into the resulting array.
	 * 
	 * If a parsing report is given, it will be filled with all the errors
	 * that are encountered when parsing those objects.
	 * 
	 * @param array<integer|string, ?string> $datas
	 * @param ?ParsingReportInterface $report
	 * @return array<integer, T>
	 */
	public function tryParseAllNullable(array $datas, ?ParsingReportInterface $report = null) : array;
	
	/**
	 * Parses the given datas into the objects they represent. If one of the
	 * given datas cannot be parsed, then a ParseThrowable is thrown.
	 * 
	 * @param Iterator<integer|string, ?string> $datas
	 * @return Iterator<integer, T>
	 * @throws ParseThrowable if the parsing process fails
	 */
	public function parseIterator(Iterator $datas) : Iterator;
	
	/**
	 * Parses the given datas into the objects they represent. If a nullable
	 * value is represented, then it will not be included into the resulting
	 * Iterator. If one of the given datas cannot be parsed, then a ParseThrowable
	 * is thrown.
	 * 
	 * @param Iterator<integer|string, ?string> $datas
	 * @return Iterator<integer, T>
	 * @throws ParseThrowable if the parsing process fails
	 */
	public function parseIteratorNullable(Iterator $datas) : Iterator;
	
	/**
	 * Parses the given datas into the objects they represent. If one the the
	 * given datas cannot be parsed, then it will silently be discarded from
	 * the resulting Iterator.
	 * 
	 * If a parsing report is given, it will be filled with Iterator the errors
	 * that are encountered when parsing those objects.
	 * 
	 * @param Iterator<integer|string, ?string> $datas
	 * @param ?ParsingReportInterface $report
	 * @return Iterator<integer, T>
	 */
	public function tryParseIterator(Iterator $datas, ?ParsingReportInterface $report = null) : Iterator;
	
	/**
	 * Parses the given datas into the objects they represent. If one of the
	 * given datas cannot be parsed, then it will silently be discarded from
	 * the resulting Iterator. If a nullable value is given, it will also not be
	 * included into the resulting Iterator.
	 * 
	 * If a parsing report is given, it will be filled with Iterator the errors
	 * that are encountered when parsing those objects.
	 * 
	 * @param Iterator<integer|string, ?string> $datas
	 * @param ?ParsingReportInterface $report
	 * @return Iterator<integer, T>
	 */
	public function tryParseIteratorNullable(Iterator $datas, ?ParsingReportInterface $report = null) : Iterator;
	
}
